# -*- encoding: utf-8 -*-
$:.push File.expand_path("../lib", __FILE__)
require "hgncrest/version"

Gem::Specification.new do |s|
    s.name          = 'hgncrest'
    s.version       = HGNCREST::VERSION
    s.date          = Time.now.strftime("%Y-%m-%d")
    s.platform      = Gem::Platform::RUBY
    
    s.summary       = "HGNC REST Ruby API"
    s.description   = "HGNC REST Ruby API"
    s.authors       = ["Natapol Pornputtapong"]
    s.email         = 'natapol.por@gmail.com'

    s.homepage      = 'http://rubygems.org/gems/hgncrest'
    s.license       = 'GPL'
    
    
    s.files         = `git ls-files`.split("\n")
    s.test_files    = `git ls-files -- {test,spec,features}/*`.split("\n")
    s.executables   = `git ls-files -- bin/*`.split("\n").map{ |f| File.basename(f) }
    s.require_paths = ["lib"]
    

end
