# 
# Bio
# Copyright (C) 2014
#
# author: Natapol Pornputtapong <natapol.por@gmail.com>
#
# Documentation: Natapol Pornputtapong (RDoc'd and embellished by William Webber)
#

# raise "Please, use ruby 1.9.0 or later." if RUBY_VERSION < "1.9.0"

$: << File.join(File.expand_path(File.dirname(__FILE__)))

#Internal library
require 'bio'
require 'open-uri'
require 'net/http'
require 'uri'
require 'json'

require 'hgncrest/version'
require 'hgncrest/connect'
require 'hgncrest/fetch'
require 'hgncrest/search'

#p HGNCREST::storedFields
#p HGNCREST::searchableFields
#p HGNCREST::search('BRAF')
#p HGNCREST::fetch('BRAF', :symbol)
#p HGNCREST::convert('BRAF', :symbol, :entrez_id)