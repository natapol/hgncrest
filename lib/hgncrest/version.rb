# 
# Exodus
# Copyright (C) 2015
#
# author: Natapol Pornputtapong <natapol.por@gmail.com>
#
# Documentation: Natapol Pornputtapong (RDoc'd and embellished by William Webber)
#

# raise "Please, use ruby 1.9.0 or later." if RUBY_VERSION < "1.9.0"


module HGNCREST
	VERSION = "1.0.4"
end
