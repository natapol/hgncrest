# 
# Bio-ensemblrest
# Copyright (C) 2014
#
# author: Natapol Pornputtapong <natapol.por@gmail.com>
#
# Documentation: Natapol Pornputtapong (RDoc'd and embellished by William Webber)
#

# raise "Please, use ruby 1.9.0 or later." if RUBY_VERSION < "1.9.0"

module HGNCREST
	
	@@fetch_cache = {}
	
	module_function
	
	# Provide search request for detail please follow http://www.genenames.org/help/rest-web-service-help#Content_type
	# 
	# @param [String] query string
	# @param [String, Symbol] specific field to fetch for. If nil, fetch based on all fields
	# 
	# @return [Array] request body
	def fetch(query, field = nil)
		if @@fetch_cache.has_key?(query)
			return @@fetch_cache[query]
		else
			result = HGNCREST.get("/fetch#{field == nil || field.empty? ? '' : "/#{field.to_s.downcase}"}/#{URI.escape(query)}")['response']['docs']
			@@fetch_cache[query] = result
			return result
		end
	end
	
	# Provide id conversion regarded to HGNC database
	# 
	# @param [String, Symbol] query string
	# @param [String, Symbol] convert from specified field as in searchableFields
	# @param [String, Symbol] convert to specified field as in storedFields
	# 
	# @return [Array] id
	def convert(query, from, to)
		fetched = HGNCREST::fetch(query, from)
		fetched != nil &&  fetched[0] != nil && fetched[0].has_key?(to.to_s.downcase) ? fetched[0][to.to_s.downcase] : ""
	end
end