# 
# Bio-ensemblrest
# Copyright (C) 2014
#
# author: Natapol Pornputtapong <natapol.por@gmail.com>
#
# Documentation: Natapol Pornputtapong (RDoc'd and embellished by William Webber)
#

# raise "Please, use ruby 1.9.0 or later." if RUBY_VERSION < "1.9.0"

module HGNCREST
	
	@@url = URI.parse('http://rest.genenames.org')
	@@http_connect = Net::HTTP.new(@@url.host, @@url.port)
	@@info = nil
	
	module_function
	
	# Connect to specific URL
	# 
	# @param [String] Service URL
	def connect(url)
		@@url = URI.parse(url)
		@@http_connect = Net::HTTP.new(@@url.host, @@url.port)
	end
	
	# Get response
	# 
	# @param [String] Specific service path
	# @return [JSON] request body
	def get(get_path)
		request = Net::HTTP::Get.new(get_path, {'Accept' => 'application/json'})
		response = @@http_connect.request(request)
		if response.code == "200"
			return JSON.parse(response.body)
		else
			raise "Invalid response: #{response.code}"
		end
	end
	
	# Download service information
	def dl_info
		@@info = HGNCREST::get('/info') if @@info == nil
	end
	
	# Return service information
	# 
	# @param [String] Specific service path
	# @return [JSON] request body
	def info
		HGNCREST::dl_info
		return @@info
	end
	
	# Return a list containing the fields that can be used to fetch and search records
	# 
	# @return [Array] searchableFields
	def searchableFields
		HGNCREST::dl_info
		return @@info["searchableFields"]
	end
	
	# Return all the fields that could possibly be returned within a fetch request
	# 
	# @return [Array] storedFields
	def storedFields
		HGNCREST::dl_info
		return @@info["storedFields"]
	end
	
end

