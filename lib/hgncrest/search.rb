# 
# Bio-ensemblrest
# Copyright (C) 2014
#
# author: Natapol Pornputtapong <natapol.por@gmail.com>
#
# Documentation: Natapol Pornputtapong (RDoc'd and embellished by William Webber)
#

# raise "Please, use ruby 1.9.0 or later." if RUBY_VERSION < "1.9.0"

module HGNCREST
	
	@@search_cache = {}
	
	module_function
	
	# Provide search request for detail please follow http://www.genenames.org/help/rest-web-service-help#Content_type
	# 
	# @param [String] query string
	# @param [String, Symbol] specific field to search for. If nil, search all fields
	# 
	# @return [Array] request body
	def search(query, field = nil)
		if @@search_cache.has_key?(query)
			return @@search_cache[query]
		else
			result = HGNCREST.get("/search#{field == nil || field.empty? ? '' : "/#{field.to_s.downcase}"}/#{URI.escape(query)}")['response']['docs']
			@@search_cache[query] = result
			return result
		end
	end
	
end